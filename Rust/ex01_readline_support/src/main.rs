extern crate rustyline;

use rustyline::error::ReadlineError;
use rustyline::Editor;

const HISTORY_FILENAME: &str = "history.txt";

fn main() {
    println!("Lispy Version 0.0.1");
    println!("Press Ctrl+c to Exit.\n");

    // We create an editor for the readline history.
    let mut readline_editor = Editor::<()>::new();
    // And then load the history, if it exists.
    if readline_editor.load_history(HISTORY_FILENAME).is_err() {
        println!("No previous history.");
    }


    loop {
        // We read some input from CLI.
        let readline = readline_editor.readline("LISPY>> ");
        // The reading of the input could fail, if a user uses special
        // key combinations. So we match against the readline Result
        // type. Result can either be some `Ok` or an some `Err`.
        match readline {
            Ok(line) => {
                readline_editor.add_history_entry(line.as_ref());
                println!("No, you are {}", line);
            },
            Err(ReadlineError::Interrupted) => {
                println!("CTRL-C");
                break
            },
            Err(ReadlineError::Eof) => {
                println!("CTRL-D");
                break
            },
            Err(err) => {
                println!("Error: {:?}", err);
                break
            }
        }
        readline_editor.save_history(HISTORY_FILENAME).expect("Could not save to readline history.");
    }
}
