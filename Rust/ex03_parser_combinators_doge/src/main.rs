extern crate pest;
#[macro_use]
extern crate pest_derive;

use pest::Parser;
use std::fs;
use std::path::Path;


// Tell Rust, that the struct below shall be created from the grammar
// in the referenced file.
#[derive(Parser)]
#[grammar = "grammar.pest"]
pub struct CSVParser;


fn read_file_to_string(path: &str) -> String {
    let path = Path::new(path);
    fs::read_to_string(path).expect("cannot read file")
}


fn main() {
    let mut doge_phrase_count: u64 = 0;
    let my_file_as_string: String = read_file_to_string("src/doge.txt");
    let parsing_result = CSVParser::parse(Rule::doge, &my_file_as_string);
    let file = parsing_result
        .expect("unsuccessful parse")
        .next()
        .unwrap();

    println!("file: {:?}", file);

    for pair in file.into_inner() {
        match pair.as_rule() {
            Rule::phrase => {
                println!("found a doge phrase while parsing");
                doge_phrase_count += 1;
            },
            Rule::phrase_delimiter => (),
            Rule::EOI => println!("found EOI while parsing"),
            _ => {
                println!("found {}:", pair);
                unreachable!();
            }
        }
    }

    println!("Number of phrases: {}", doge_phrase_count);
}
