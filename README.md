# Conventions

* The book uses the C99 standard. This means we compile using the `-std=c99` parameter.
* We print all warnings, to avoid getting into bad practices. This means we compile using the `-Wall` flag.
* When we compile, we specify an output file with a readable name using the `-o` parameter.
* The names of C code files are all lower case letters and a few special symbols (possibly only the underscore) to increase readability.
* The C code files have the file ending `.c`.
* The output files of compilations of C code files do not have a file ending.

# Compilation

## Example commands

* For GNU compiler collection:

  ``` sh
  gcc -std=c99 -Wall hello_world.c -o hello_world
  ```

  * With warning for unused parameters:

    ``` sh
    gcc -std=c99 -Wall -Wunused-parameter hello_world.c -o hello_world
    ```

# Hints

* Valgrind might help finding memory leaks.

# Progess in the book

* on page `19` out of `206` (end of learning material in the book).

# Ideas

* Maybe I can do the whole book in _Rust_ or/and _Guile_? In Guile it would probably be easier to get the ideas right, as it probably already has many of the semantics of the Lisp I am writing. In Rust I would have static type checking and loads of type inference. Maybe Rust would prevent many kinds of mistakes, that I would make in C.
